var express = require('express');
var bodyParser = require('body-parser')
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.use(express.static(__dirname));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}))

io.on('connection', () =>{
    console.log('a user is connected')
})

app.post('/data', (req, res) => {
    let data = req.body.data
    io.emit('message', data);

    res.send(data)
})



// Route of index
app.get("/", (req, res) => {
    res.sendFile(path.join(__dirname + "/index.html"));
  });


var server = http.listen(3000, () => {
  console.log('server is running on port', server.address().port);
});